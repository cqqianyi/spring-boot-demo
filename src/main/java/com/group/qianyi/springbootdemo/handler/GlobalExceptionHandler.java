//package com.group.qianyi.springbootdemo.handler;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//
///**
// * Created by qianyi on 2017/11/21.
// */
//@ControllerAdvice
//public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
//
//
//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<Error> handle(RuntimeException ex){
//        Error error = new Error(HttpStatus.BAD_REQUEST.toString(), null);
//        return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
//    }
//
//}
